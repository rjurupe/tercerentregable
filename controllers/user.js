var requestJSON = require('request-json');
require('dotenv').config();

const baseMLabURL="https://api.mlab.com/api/1/databases/techu49db/collections/";
const apikeyMLab="apiKey="+process.env.MLAB_API_KEY;

function ListarUsuarios (req, res) {
   console.log("Listar Usuarios");

   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");

    var queryString = 'f={"_id":0}&';

    console.log(apikeyMLab);

    httpClient.get('users?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
       if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            console.log(body.length);
           response = body;
          } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
      }
        res.send(response);
      });
};

function Login(req,res)
{
  console.log(req.params);
  let email=req.body.email;
  let pass=req.body.password;
  let queryString='q={"email":"'+email+'","password":"'+pass+'"}&';
  let limFilter='l=1&'

  var httpClient = requestJSON.createClient(baseMLabURL);
  console.log("Cliente HTTP mLab creado.");

  httpClient.get('users?'+queryString+limFilter+apikeyMLab,
      function(error, respuestaMLab, body) {
        if (!error){
            console.log("No se encontro error en el get");
            if ( body.length==1){
                console.log("Entro cuando es uno en el get");
                var logged='{"$set":{"logged":true}}';

                //  var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
                console.log(logged);
                //  clienteMlab.put(baseMLabURL +'users?' + queryStringID + apikeyMLab, JSON.parse(cambio),

                httpClient.put('users?q={"idUser":'+ body[0].idUser+'}&'+apikeyMLab,JSON.parse(logged),
                function(errorPut,respuestaPut,bodyPut) {
                  if (!errorPut){
                    console.log("No se encontro error en el put");
                    if (bodyPut.n==1){
                      res.send({'msg':'Login correcto','user': body[0].email, 'userId':body[0].id,'name':body[0].first_name});
                    }else{
                      res.send({'msg':'No actualizo nada'});
                    }
                    //  res.send({'msg':'Login correcto'});
                  }
                  else{res.send({'msg':'No se pudo hacer login'});}
                });
              }

            }else{
              res.send({"msg":"No existe usuario"});
            }
          });
          }

          function Logout(req,res)
          {            
            let email=req.body.email;
            let queryString='q={"email":"'+email+'"}&';
            let limFilter='l=1&'

            var httpClient = requestJSON.createClient(baseMLabURL);
            console.log("Cliente HTTP mLab creado.");

            httpClient.get('users?'+queryString+limFilter+apikeyMLab,
                function(error, respuestaMLab, body) {
                  if (!error){
                      console.log("No se encontro error en el get");
                      if ( body.length==1){
                          console.log("Entro cuando es uno en el get");
                          var logged='{"$unset":{"logged":""}}';

                          httpClient.put('users?q={"idUser":'+ body[0].idUser+'}&'+apikeyMLab,JSON.parse(logged),
                          function(errorPut,respuestaPut,bodyPut) {
                            if (!errorPut){
                              console.log("No se encontro error en el put");
                              if (bodyPut.n==1){
                                res.send({'msg':'Logout correcto','user': body[0].email, 'userId':body[0].id,'name':body[0].first_name});
                              }else{
                                res.send({'msg':'No actualizo nada'});
                              }
                                                          }
                            else{res.send({'msg':'No se pudo hacer logout'});}
                          });
                        }

                      }else{
                        res.send({"msg":"No existe usuario"});
                      }
                    });
                    }





                    // let j=0;
                    // let deslogueado=false;
                    // let msg='';
                    //
                    // while (j<user_file.length && (!deslogueado))
                    // {
                    //   if(user_file[j].logged==undefined){
                    //     console.log('no se encuentra logeado'+user_file[j].email);}
                    //   else {
                    //     if ( user_file[j].logged==true && user_file[j].email==req.body.email){
                    //        deslogueado=true;
                    //        console.log('es usuario solicitado');
                    //        user_file[j].logged=false;}
                    //     else
                    //       {console.log('no es usuario solicitado'+user_file[j].email);}
                    //     }
                    //   j++;
                    // }
                    // res.send(!deslogueado?{"msg":"No se encuentra logueado"}:{"msg":"Usuario sale del aplicativo"});
                    // });




module.exports={
      ListarUsuarios,
      Login,
      Logout

    }
