var express = require('express');
var bodyParser = require("body-parser");

var app = express();
const user_controller=require("./controllers/user");

require('dotenv').config();

app.use(bodyParser.json());

var port=process.env.Port|| 3000;

const URL_BASE=process.env.URLbase;

//Peticion GET
app.get(URL_BASE+'users',user_controller.ListarUsuarios);

//Peticion Login
app.post(URL_BASE+'login',user_controller.Login);

//Peticiòn Logout
app.post(URL_BASE+'logout',user_controller.Logout);

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
  console.log(process.env.port);

});
